/**********************************************
 *  Modensie                                  *
 *  École Normale Supérieure de Lyon          *
 *  Département Informatique - M1             *
 **********************************************/

-- -----------------------------------------------------
-- Registrations
-- -----------------------------------------------------
INSERT INTO user_account(username,password,name,birthdate,gender,email_address,role)
VALUES
('melanie_laurent', 'qwerty123', 'Mélanie Laurent', '1995-7-04', 'F', 'melanie.laurent@ens-lyon.fr', 'student'),
('emman_beart', 'hf367g3d', 'Emmanuelle Béart', '1996-11-15', 'M', 'emmanuelle.beart@ens-lyon.fr', 'student'),
('bernblier', '34jtv45ug8', 'Bernard Blier', '1996-6-14', 'M', 'bernard.blier@ens-lyon.fr', 'student'),
('laet_casta', '0-o5op4ky59', 'Laetitia Casta', '2001-3-01', 'F', 'laetitia.casta@ens-lyon.fr', 'student'),
('irene_jacob', 'oi4893jf', 'Irène Jacob', '1994-3-14', 'F', 'irene.jacob@ens-lyon.fr', 'student'),
('jean_dujardin', '43f34f3e', 'Jean Dujardin', '1996-6-24', 'M', 'jean.dujardin@ens-lyon.fr', 'student'),
('louis_funes', 'lturb894my', 'Louis de Funès', '1994-3-18', NULL, 'louis.funes@ens-lyon.fr', 'student'),
('alexia_portal', '34ergv37', 'Alexia Portal', '2003-12-10', 'F', 'alexia.portal@ens-lyon.fr', 'student'),
('yvonprint2', 'gj5468h5', 'Yvonne Printemps', '2000-4-03', 'F', 'yvonne.printemps@ens-lyon.fr', 'student'),
('herve_villec', '123456', 'Hervé Villechaize', '1974-1-09', 'M', 'hervé.villechaize@ens-lyon.fr', 'teacher'),
('audrey_tautou', 'regvdfgh54', 'Audrey Tautou', '1956-2-19', 'F', 'audrey.tautou@ens-lyon.fr', 'teacher'),
('olivier_martinez', '34v645yh', 'Olivier Martinez', '1962-2-22', 'F', 'olivier.martinez@ens-lyon.fr', 'teacher');

INSERT INTO course(name,password)
VALUES
('Computer Algebra', 'welcometoCA2021'),
('Cryptography and Security', 'welcometoCS2021'),
('Data Bases and Data Mining', 'welcometoDBDM2021'),
('Programs and Proofs', NULL);

INSERT INTO participant_course(course_id,user_id)
VALUES
((SELECT course_id FROM course WHERE name = 'Computer Algebra'),(SELECT user_account_id FROM user_account WHERE username = 'herve_villec')),
((SELECT course_id FROM course WHERE name = 'Computer Algebra'),(SELECT user_account_id FROM user_account WHERE username = 'audrey_tautou')),
((SELECT course_id FROM course WHERE name = 'Computer Algebra'),(SELECT user_account_id FROM user_account WHERE username = 'melanie_laurent')),
((SELECT course_id FROM course WHERE name = 'Computer Algebra'),(SELECT user_account_id FROM user_account WHERE username = 'emman_beart')),
((SELECT course_id FROM course WHERE name = 'Computer Algebra'),(SELECT user_account_id FROM user_account WHERE username = 'laet_casta')),
((SELECT course_id FROM course WHERE name = 'Cryptography and Security'),(SELECT user_account_id FROM user_account WHERE username = 'herve_villec')),
((SELECT course_id FROM course WHERE name = 'Cryptography and Security'),(SELECT user_account_id FROM user_account WHERE username = 'melanie_laurent')),
((SELECT course_id FROM course WHERE name = 'Data Bases and Data Mining'),(SELECT user_account_id FROM user_account WHERE username = 'audrey_tautou')),
((SELECT course_id FROM course WHERE name = 'Programs and Proofs'),(SELECT user_account_id FROM user_account WHERE username = 'olivier_martinez'));

INSERT INTO assignment(course_id,user_id,name,type,description,date)
VALUES
((SELECT course_id FROM course WHERE name = 'Computer Algebra'),(SELECT user_account_id FROM user_account WHERE username = 'herve_villec'),'Numbers',0,'Numbers, variables and mathematical expressios',NOW()),
((SELECT course_id FROM course WHERE name = 'Computer Algebra'),(SELECT user_account_id FROM user_account WHERE username = 'audrey_tautou'),'Simplification',0,'Basic rules of differentiation',SUBDATE(NOW(),1)),
((SELECT course_id FROM course WHERE name = 'Computer Algebra'),(SELECT user_account_id FROM user_account WHERE username = 'herve_villec'),'Equality',1,'Rational fractions exercises',SUBDATE(NOW(),2)),
((SELECT course_id FROM course WHERE name = 'Cryptography and Security'),(SELECT user_account_id FROM user_account WHERE username = 'melanie_laurent'),'Classic cryptography',2,'The main classical ciphers (transposition ciphers)',NOW()),
((SELECT course_id FROM course WHERE name = 'Programs and Proofs'),(SELECT user_account_id FROM user_account WHERE username = 'olivier_martinez'),'Programming language theory',1,'The Curry–Howard correspondence',SUBDATE(NOW(),1)),
((SELECT course_id FROM course WHERE name = 'Programs and Proofs'),(SELECT user_account_id FROM user_account WHERE username = 'olivier_martinez'),'Origin, scope, and consequences',0,'he beginnings of the Curry–Howard correspondence',NOW());

INSERT INTO meeting(course_id,name,room,start_time,end_time,description,link)
VALUES
((SELECT course_id FROM course WHERE name = 'Computer Algebra'),'Numbers and Simplifications Part 2','BBB Online',DATE_SUB(NOW(), INTERVAL 3 HOUR),NOW(),'Basic computations','https://www.bbb.org/computer_algebra'),
((SELECT course_id FROM course WHERE name = 'Computer Algebra'),'Numbers and Simplifications Part 1','BBB Online',SUBDATE(DATE_SUB(NOW(), INTERVAL 5 HOUR),1),SUBDATE(DATE_SUB(NOW(), INTERVAL 2 HOUR),1),'Basic computations','https://www.bbb.org/computer_algebra'),
((SELECT course_id FROM course WHERE name = 'Cryptography and Security'),'Cryptography Q&A','BBB Online',SUBDATE(DATE_SUB(NOW(), INTERVAL 7 HOUR),2),SUBDATE(DATE_SUB(NOW(), INTERVAL 4 HOUR),2),'Ciphers and other stuff','https://www.bbb.org/cryptography'),
((SELECT course_id FROM course WHERE name = 'Programs and Proofs'),'Online Exercises','BBB Online',SUBDATE(DATE_SUB(NOW(), INTERVAL 3 HOUR),1),SUBDATE(NOW(),1),'Easy and Complex Exercises','https://www.bbb.org/programs_and_proofs'),
((SELECT course_id FROM course WHERE name = 'Programs and Proofs'),'Online Exercises','BBB Online',SUBDATE(DATE_SUB(NOW(), INTERVAL 12 HOUR),3),SUBDATE(DATE_SUB(NOW(), INTERVAL 9 HOUR),3),'Easy and Complex Exercises','https://www.bbb.org/programs_and_proofs');
