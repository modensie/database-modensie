/**********************************************
 *  Modensie                                  *
 *  École Normale Supérieure de Lyon          *
 *  Département Informatique - M1             *
 **********************************************/

-- -----------------------------------------------------
-- Queries
-- -----------------------------------------------------
SELECT * FROM user_account;
SELECT * FROM course;
SELECT * FROM participant_course;
SELECT * FROM assignment;
SELECT * FROM meeting;

-- Get all courses
SELECT name FROM course;

-- All courses of a user
SELECT course.name FROM course
	JOIN participant_course ON course.course_id = participant_course.course_id
    JOIN user_account ON user_account.user_account_id = participant_course.user_id
	WHERE user_account.username = 'herve_villec';
 
 -- All courses not followed by a user
SELECT course.name FROM course
WHERE course_id NOT IN(
	SELECT course.course_id FROM course
		JOIN participant_course ON course.course_id = participant_course.course_id
		JOIN user_account ON user_account.user_account_id = participant_course.user_id
		WHERE user_account.username = 'herve_villec'
    );

SELECT * FROM course;
SELECT course.name FROM course WHERE course_id NOT IN (SELECT course.course_id FROM course JOIN participant_course ON course.course_id = participant_course.course_id
            JOIN user_account ON user_account.user_account_id = participant_course.user_id
            WHERE user_account.username = 'herve_villec');
            
-- Delete course
DELETE FROM course WHERE name = 'Computer Algebra';

-- All participants of a course
SELECT user_account.* FROM user_account
	JOIN participant_course ON user_account_id = user_id
	JOIN course ON course.course_id = participant_course.course_id
	WHERE course.name = 'Computer Algebra';
            
-- All students of a course
SELECT username FROM user_account
	JOIN participant_course ON user_account_id = user_id
    JOIN course ON course.course_id = participant_course.course_id
	WHERE user_account.role ='student' AND course.name = 'Computer Algebra';

-- Check if user is assigned to a course
SELECT course.* FROM course
	JOIN participant_course ON course.course_id = participant_course.course_id
    JOIN user_account ON user_account.user_account_id = participant_course.user_id
	WHERE user_account.username = 'herve_villec' AND course.name = 'Computer Algebra';

-- Check if assignment is assigned to a course
SELECT assignment.* FROM assignment
	JOIN course ON course.course_id = assignment.course_id
	WHERE assignment.name = 'Numbers' AND course.name = 'Computer Algebra';
    
-- All assignmnts of a course
SELECT assignment.name, user_account.name AS teacher, date, type, description FROM assignment
	JOIN course ON course.course_id = assignment.course_id
    JOIN user_account ON user_account_id = assignment.user_id
	WHERE course.name = 'Computer Algebra';

-- Find an assignment assigned to a course
SELECT assignment.* FROM assignment JOIN course ON course.course_id = assignment.course_id
WHERE assignment.name = 'Numbers' AND course.name = 'Computer Algebra';

-- All meetings of a user
SELECT course.name AS courseName,meeting.* FROM meeting
	JOIN course ON meeting.course_id = course.course_id
	JOIN participant_course ON course.course_id = participant_course.course_id
	JOIN user_account ON user_account.user_account_id = participant_course.user_id
    WHERE user_account.username = 'melanie_laurent'