/**********************************************
 *  Modensie                                  *
 *  École Normale Supérieure de Lyon          *
 *  Département Informatique - M1             *
 **********************************************/

-- --------------------------------------------------------
-- Create Database
-- --------------------------------------------------------
DROP DATABASE IF EXISTS ModensiePlatformDB;
CREATE DATABASE IF NOT EXISTS ModensiePlatformDB;
USE ModensiePlatformDB;

-- --------------------------------------------------------
-- User Privileges
-- --------------------------------------------------------
DROP USER IF EXISTS 'modensie_user';
CREATE USER 'modensie_user'@'%' IDENTIFIED BY 'ENSpassM123';
GRANT ALL ON ModensiePlatformDB.* TO 'modensie_user'@'%';
-- FLUSH PRIVILEGES;
-- SHOW GRANTS FOR 'modensie_user'@localhost;

-- Refresh Tables
DROP TABLE IF EXISTS meeting;
DROP TABLE IF EXISTS assignment;
DROP TABLE IF EXISTS participant_course;
DROP TABLE IF EXISTS course;
DROP TABLE IF EXISTS user_account;

-- -----------------------------------------------------
-- Tables Creation
-- -----------------------------------------------------
-- Table user_account
DROP TABLE IF EXISTS user_account;
CREATE TABLE user_account(
user_account_id binary(16) UNIQUE PRIMARY KEY,
username varchar(64) UNIQUE NOT NULL,
password varchar(64) NOT NULL,
name varchar(64) NOT NULL,
birthdate date NOT NULL,
gender varchar(1),
email_address varchar(64) NOT NULL,
role varchar(64) NOT NULL
);

-- Table course
DROP TABLE IF EXISTS course;
CREATE TABLE course(
course_id binary(16) UNIQUE PRIMARY KEY,
name varchar(64) UNIQUE NOT NULL,
password varchar(64)
);

-- Table participant_course
DROP TABLE IF EXISTS participant_course;
CREATE TABLE participant_course(
participant_course_id binary(16) UNIQUE PRIMARY KEY,
course_id binary(16) NOT NULL,
user_id binary(16) NOT NULL
);

-- Table assignment
DROP TABLE IF EXISTS assignment;
CREATE TABLE assignment(
assignment_id binary(16) UNIQUE PRIMARY KEY,
course_id binary(16) NOT NULL,
user_id binary(16) NOT NULL,
name varchar(64) NOT NULL,
date datetime NOT NULL DEFAULT now(),
type int(1) NOT NULL,	-- 0(lesson)/1(exercise)/2(news)
description varchar(256)
);

-- Table meeting
DROP TABLE IF EXISTS meeting;
CREATE TABLE meeting(
meeting_id binary(16) UNIQUE PRIMARY KEY,
course_id binary(16) NOT NULL,
name varchar(64) NOT NULL,
room varchar(64),
start_time datetime NOT NULL,
end_time datetime NOT NULL,
description varchar(256),
link varchar(64)
);

-- -----------------------------------------------------
-- FOREIGN KEYS
-- -----------------------------------------------------
ALTER TABLE participant_course 
 	ADD FOREIGN KEY (course_id) REFERENCES course(course_id),
 	ADD FOREIGN KEY (user_id) REFERENCES user_account(user_account_id);
ALTER TABLE assignment 
 	ADD FOREIGN KEY (course_id) REFERENCES course(course_id),
 	ADD FOREIGN KEY (user_id) REFERENCES user_account(user_account_id);
ALTER TABLE meeting 
 	ADD FOREIGN KEY (course_id) REFERENCES course(course_id);

-- -----------------------------------------------------
-- CONSTRAINTS
-- -----------------------------------------------------
/* Referential integrity constraints */
ALTER TABLE participant_course ADD CONSTRAINT participant_course_Constraint
FOREIGN KEY (course_id) REFERENCES course(course_id) 
ON DELETE CASCADE;

ALTER TABLE assignment ADD CONSTRAINT assignment_Constraint
FOREIGN KEY (course_id) REFERENCES course(course_id) 
ON DELETE CASCADE;

ALTER TABLE meeting ADD CONSTRAINT meeting_Constraint
FOREIGN KEY (course_id) REFERENCES course(course_id) 
ON DELETE CASCADE;

/* Domain constraints */
ALTER TABLE user_account ADD CONSTRAINT UserAccount_Constraint_Gender
CHECK(gender = 'M' OR gender = 'F');

ALTER TABLE user_account ADD CONSTRAINT UserAccount_Constraint_Email
CHECK(email_address LIKE '%_@__%.__%');

-- -----------------------------------------------------
-- Triggers
-- -----------------------------------------------------
DROP FUNCTION IF EXISTS uuid_to_bin;
DROP FUNCTION IF EXISTS bin_to_uuid;

delimiter #
CREATE FUNCTION bin_to_uuid(b binary(16))
RETURNS char(36) DETERMINISTIC
BEGIN
  DECLARE HEX char(32);
  SET HEX = HEX(b);
  RETURN LOWER(CONCAT(LEFT(HEX, 8), '-', MID(HEX, 9,4), '-', MID(HEX, 13,4), '-', MID(HEX, 17,4), '-', RIGHT(HEX, 12)));
END;
#
delimiter ;

delimiter #
CREATE FUNCTION uuid_to_bin(uuid char(36))
returns binary(16) DETERMINISTIC
BEGIN
  RETURN UNHEX(REPLACE(uuid, '-', ''));
END;
#
delimiter ;

DELIMITER #
CREATE TRIGGER GenerateUUID_user_account BEFORE INSERT ON user_account
FOR EACH  ROW 
BEGIN 
    SET NEW.user_account_id = uuid_to_bin(UUID());
END;
#
DELIMITER ;

DELIMITER #
CREATE TRIGGER GenerateUUID_course BEFORE INSERT ON course
FOR EACH  ROW 
BEGIN 
    SET NEW.course_id = uuid_to_bin(UUID());
END;
#
DELIMITER ;

DELIMITER #
CREATE TRIGGER GenerateUUID_participant_course BEFORE INSERT ON participant_course
FOR EACH  ROW 
BEGIN 
    SET NEW.participant_course_id = uuid_to_bin(UUID());
END;
#
DELIMITER ;

DELIMITER #
CREATE TRIGGER GenerateUUID_assignment BEFORE INSERT ON assignment
FOR EACH  ROW 
BEGIN 
    SET NEW.assignment_id = uuid_to_bin(UUID());
END;
#
DELIMITER ;

DELIMITER #
CREATE TRIGGER GenerateUUID_meeting BEFORE INSERT ON meeting
FOR EACH  ROW 
BEGIN 
    SET NEW.meeting_id = uuid_to_bin(UUID());
END;
#
DELIMITER ;
