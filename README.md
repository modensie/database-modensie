# Database Modensie

Database for Modensie Platform (SQL)

**Server on localhost:3306**

## Setup
If you're using **mysql/mariadb** server, change the `spring.datasource.url` field of the Backend Application _(Backend Modensie\Modensie\src\main\resources\application.properties)_ to your preferred server.

For **MySQL**: `spring.datasource.url=jdbc:mysql://localhost:3306/ModensiePlatformDB`

For **MariaDB**: `spring.datasource.url=jdbc:mariadb://localhost:3306/ModensiePlatformDB`

## Create the Database for Modensie (Example using MariaDB)
1. Clone the Database Modensie repository `git clone https://gitlab.com/modensie/database-modensie.git`
2. Uncomment the **User Privileges** lines (17-21) from the _database_modensie_initialization.sql_ file, in order to create the special user used for the backend application, which uses jdbc.
3. Run `mysql -u root -p` or `mysql -u [user] -p` in your Terminal to have access to you database.
4. Type `source database_modensie_initialization.sql` in order to run the queries for creating the tables/users/constraints/dependencies.
5. Optionally type `source database_modensie_registrations_opt.sql` to populate the tables with data.